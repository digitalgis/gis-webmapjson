
## Webmap Configs

This repo is a backup of the webmaps from Watercare internal GIS applications. These files are to be used in conjunction with [AGOL Assistant](https://ago-assistant.esri.com/)  to update the configs of the webmaps.

There are two branches, each branch will configure with the Portal environment

|Branch|  Portal ENV|
|--|--|
| release/Development | https://wsldctdgweb.water.internal/portal/home/ |
| master|  https://wsldctpgweb.water.internal/portal/home/|


 The list of filenames are below:

|filename  |  App Name|
|--|--|
| nvge.json |  Netview |
| nvcu.json |  Netview for Customer |
| nvdt.json |  Netview for Developer Services|
| nvmd.json |  Netview for Major Development|
| nvin.json |  Netview for Planning|
| nvms.json |  Netview for MSN|
| nvop.json |  Netview for Operations|
| nvpr.json |  Netview for Property|
| nvwo.json |  Netview for Works Over|
| tpw.json  |  Te Puna Wai|
